$(document).ready(function(){
	$('.slider-general').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,

	});

	$('.slider-events').slick({

		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 780,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 549,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
    	]
	});
});