 <div class="row" id="id10"  >
		<div class="vertical-align-container vac-id10">
			<div  class="vertical-align-content text-center vac-id10">
				<div class="small-12 small-centered columns">
					<div class="row">
						<div class="small-12 small-centered medium-uncentered medium-6 large-7 columns">
							<h5>We have designed this page inspired by the experiences we have lived in IEEE, looking for leave a legacy to our beloved student branch and encouraging you, through testimonies and facts to join us in this great adventure. We have worked with all our energy and creativity in this page, we hope you enjoy it and liked it</h5>
							<h5 class="text-right"><span>Sergio Gutiérrez, Cristian Quintero - Design Department IEEE-UD</span></h5>
						</div>
						<div class="small-12 small-centered medium-uncentered medium-6 large-5 columns">
							<div class="row row-logos">
								<div class="small-4 medium-4 columns">
									<a href="" title=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/100_INGLES.png" alt=""></a>
								</div>
								<div class="small-4 medium-4 columns">
									<a href="" title=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/LogoRama.png" alt=""></a>
								</div>
								<div class="small-4 medium-4 columns">
									<a href="" title=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/IEEE.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php wp_footer(); ?>

<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/bower_components/jquery/dist/jquery.min.js"></script>
<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/bower_components/foundation/js/foundation.min.js"></script>
<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/bower_components/foundation/js/foundation/foundation.magellan.js"></script>
<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/js/jquery.stellar.min.js"></script>
<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/js/app.js"></script>
<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/js/slider-host/slick.min.js"></script>
<script src="http://ieee.udistrital.edu.co/wp-content/themes/IEEE_UD/js/slider.general.js"></script>
</body>
</html>
