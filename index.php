<?php get_header(); ?>

<section id="main">
	<div id="id1" class="row cbackground clearfix" >
		<div class="insignia">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/InsigniaPuestoEN.png" alt="">
		</div>

		<div class="vertical-align-container vac-id1">
			<div  class="vertical-align-content vac-id1"  data-stellar-ratio="1.5">
				<div class="row">
					<div class="small-3 small-centered columns text-center">
						<img class="img-rama" src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/ChamizoCircuilar.png" alt="">
					</div>
				</div>
				<div class="row">
					<div class="small-6 small-centered columns text-center img-logo">
						<img class="img-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/EnjoyTheExperienceShadow.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row row-social">
			<div class="small-12 small-centered columns buttons">
				<a href="https://www.facebook.com/RamaIEEEud" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Facebook.png">
				</a>
				<a href="https://twitter.com/IEEEUD" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Twitter.png">
				</a>
				<a href="https://instagram.com/ieee.ud/" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Instagram.png">
				</a>
				<a href="https://www.youtube.com/user/RamaIEEEud" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/YouTube.png">
				</a>
			</div>
		</div>

		<!-- <div class="row row-lenguaje">
			<div class="small-12 small-centered columns">
				<a href="http://ieee.ud">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/BotonES.png">
				</a>
				<a href="http://ieee.ud/EN">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/BotonEN.png">
				</a>
			</div>
		</div> -->
	</div>

	<div class="row" id="id2" >
		<div class="vertical-align-container vac-id2 vac">
			<div  class="vertical-align-content text-center vac-id2">
				<div class="small-12  medium-8 small-centered  columns " >
					<h3 class="titles">We are a team</h3>
					<h4>Stablishing a way to contribute to the development of our society</h4>
					<br>
					<p >We are part of the District University of Bogota and as team, we share a common goal: Contribute to the professional development of students and the educational community, providing opportunities for the development of leadership and interdisciplinary work, which gives as result volunteers committed to their work and allows us to be recognized as one of the most outstanding branches at the national, regional and global level</p>
					<br>
					<a href="/en/team/" class="button gris">DISCOVER MORE</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row cbackground" id="id3" >
		<div class="vertical-align-container vac-id3 vac">
			<div  class="vertical-align-content text-center vac-id3">
				<div class="small-11 medium-8 small-centered columns txt-blanco" >
					<h3 class="titles">Over 50 Years</h3>
					<h4>Providing leadership to the Society</h4>
					<br>
					<p >The first man on the moon, the invention of the Intel 4004 microprocessor, the launch of Telstar I and the first TV satellite transmission were some of the events that marked a very important decade for our society and it was the same in which was founded our branch</p>
					<br>
					<a href="/en/history/" class="button blanco">OUR HISTORY</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="id4">
		<div class="vertical-align-container vac-id4  vac">
			<div  class="vertical-align-content text-center vac-id4">
				<div class="small-12 medium-11 small-centered columns" >
					<h3 class="titles">Conferences and Event</h3>
					<h4>Promoting the development of investigacion and innovation</h4>
					<br>
					<div class="row">
						<div class="small-11 small-centered medium-4 medium-uncentered columns">
							<a href="http://ieee.udistrital.edu.co/rap/" target="_blank"><img src="http://ieee.ud/wp-content/uploads/2015/06/RobotAlParque.jpg" alt="Robot Al Parque 2015" class="img-event"></a>
							<h3 class="stitle-event">Robot Park® 2015</h3>
							<p>
								Be part of one of the best robotics contest in Bogotá, don’t miss this amazing event that comes on his tenth version
							</p>

						</div>
						<div class="small-11 small-centered medium-4 medium-uncentered columns">
							<img  src="http://ieee.udistrital.edu.co/wp-content/uploads/2015/06/OpenCongress.jpg" alt="Open Congress® 2015" class="img-event">
							<h3 class="stitle-event">Open Congress® 2015</h3>
							<p >An event that promotes interdisciplinary on engineering knowledge to help improve the life level of Colombians</p>
						</div>
						<div class="small-11 small-centered medium-4 medium-uncentered columns">
							<a href="http://ieee.udistrital.edu.co/wea/" target="_blank"><img  src="http://ieee.udistrital.edu.co/wp-content/uploads/2015/06/WEA.jpg" alt="Workshop on Engineering." class="img-event"></a>
							<h3 class="stitle-event">Workshop on Engineering</h3>
							<p >
								Promotes science. It lets us know the quality of our research with exposing the best jobs in the region.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row cbackground" id="id5" >
		<div class="vertical-align-container vac-id5 vac">
			<div  class="vertical-align-content text-center vac-id5">
				<div class="small-12  small-centered medium-6 columns txt-blanco" >
					<h3 class="titles">Vivencias™</h3>
					<h4 >Our Blog</h4>
					<br>
					<p >Amazing trips, unmatched conferences, unique memories and everything that happens in just one place.Click here and read what we do every single day</p>
					<br>
					<a href="/en/vivencias" class="button blanco">READ NOW</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="id6">
		<div class="vertical-align-container vac-id6 vac">
			<div  class="vertical-align-content text-center vac-id6">
				<div class="small-12 small-centered columns">
					<h3 class="titles">Rama Tv™</h3>
					<h4>Making creativity an essential part</h4>
					<br>

					<iframe width="1024" height="580" src="https://www.youtube-nocookie.com/embed/EWHegJjpKHA?hd=1&amp;rel=0&amp;autohide=1&amp;showinfo=0" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" yplugin="true" style="display: block;"></iframe>
					<br>
					<br>
					<a href="https://www.youtube.com/user/RamaIEEEud" target="_blank" class="button gris">WATCH MORE</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row cbackground" id="id7">
		<div class="vertical-align-container vac-id7 vac">
			<div  class="vertical-align-content text-center vac-id7">
				<div class="small-12 small-centered medium-7 columns txt-blanco" >
					<h3 class="titles">IEEE</h3>
					<h4 >The world’s largest professional association</h4>
					<br>
					<p >
						Take the next step and live a unique experience beyond academic, in which you can apply the skills you learn inside and outside the classroom so you can lead and impact the world around you. Take advantage of all the benefits that IEEE has for you and make new friends who contribute to your professional future
					</p>
					<br>
					<a href="http://ieee.org" target="_blank" class="button blanco">LEARN MORE</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="id8">
		<div class="vertical-align-container vac-id8 vac" id="chapters">
			<div  class="vertical-align-content text-center vac-id8">
				<div class="small-12 small-centered medium-8 columns">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/JoinUsLogo.png" alt="">
					<br>
					<br>
					<p>
						When you enter our branch, in addition to a workgroup you'll find a family that will always be ready to support you to achieve your goals; for this reason we would love you to be part of amazing projects
					</p>
				</div>
				<div class="small-12 small-centered medium-11 columns ">
					<br>
					<div class="row">
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="https://www.facebook.com/AESSUD?fref=ts" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/AESS.png" alt="Chapter - AESS - IEEEUD">
							</a>
							<p>AESS</p>
						</div>

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/CAS.png" alt="Chapter - CAS - IEEEUD">
							</a>
							<p>CAS</p>
						</div>

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/Computer.png" alt="Chapter - COMPUTER - IEEEUD">
							</a>
							<p>COMPUTER</p>
						</div>

					</div>
					<div class="row">

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/ComSoc.png" alt="Chapter - COMSOC - IEEEUD">
							</a>
							<p>COMSOC</p>
						</div>

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="http://ieee.udistrital.edu.co/en/css/" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/CSS.png" alt="Chapter - CSS - IEEEUD">
							</a>
							<p>CSS</p>
						</div>

<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/EDS.png" alt="Chapter - EDS - IEEEUD">
							</a>
							<p>EDS</p>
						</div>

					</div>
					<div class="row">


						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/EMB.png" alt="Chapter - EMB - IEEEUD">
							</a>
							<p>EMB</p>
						</div>

						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="https://www.facebook.com/pages/Capitulo-Geociencias-GRSS-UD/1436495919974444?fref=ts" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/GRSS.png" alt="Chapter - GRSS - IEEEUD">
							</a>
							<p>GRSS</p>
						</div>
						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="http://ieee.udistrital.edu.co/en/ias/" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/IAS.png" alt="Chapter - IAS - IEEEUD">
							</a>
							<p>IAS</p>
						</div>
http://ieee.udistrital.edu.co/en/ias/
					</div>
					<div class="row">

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="IAS Chapter">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/PELS.png" alt="Chapter - PELS - IEEEUD">
							</a>
							<p>PELS</p>
						</div>
					
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="https://www.facebook.com/PESIEEEUD?fref=ts" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/PES.png" alt="Chapter - PES - IEEEUD">
							</a>
							<p>PES</p>
						</div>
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="http://ieee.udistrital.edu.co/ras/" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/RAS.png" alt="Chapter - RAS - IEEEUD">
							</a>
							<p>RAS</p>
						</div>

					</div>
					<div class="row">

						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="https://www.facebook.com/WIEUDistrital?fref=ts" title="">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/WIE.png" alt="Capitulo - WIE - IEEEUD">
							</a>
							<p>WIE</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<?php
$mypod = pods( 'links' );
$mypod = pods( 'links', $id_or_slug );
$params = array('limit' =>15);
$mypod = pods( 'links', $params );
$mypod = pods( 'links' )->find( $params );
$mypod = pods( 'links' );
$mypod->find( $params );

?>
<div class="row cbackground" id="id8a">
<div class="vertical-align-container vac-id8a vac">
<div  class="vertical-align-content text-center vac-id8a">
<div class="small-12  small-centered  columns" >
<div class="row">
<?php while ( $mypod->fetch() ) { ?>
<div class="small-11 small-centered  medium-6 medium-uncentered columns">
<div class="row">
<a href="<?= $mypod->display('link') ?>">
<div class="small-8 columns">

<p class="titulo"><?= $mypod->display('titulo') ?></p>
<p><?= $mypod->display('descripcion') ?></p>
</div>
<div  class="small-4 columns icom">
<div class="small-6 small-left columns cont_icom">
<i class="fa fa-<?= $mypod->display('icono') ?>"></i>
</div>
</div>
</a>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
</div>

	<div class="row cbackground" id="id9">
		<div class="vertical-align-container vac-id9">
			<div  class="vertical-align-content text-center vac-id9">
				<div class="small-12 small-centered medium-10 columns">
					<div class="row">
						<div class="small-12 medium-4 left columns recuadro" id="download-center">
							<h3>Download Center</h3>
							<p>Besides documents, records, papers, projects and other academic files, we have designed a series of Wallpapers for your devices so you can carry the #IEEExperience always with you</p>
							<a href="/en/download-center/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Iconos/MultiDevice.png" alt="Download"></a>
						</div>

						<div class="small-12 medium-4 columns" id="timeline-tw">
							<div>
								<a class="twitter-timeline" href="https://twitter.com/ieeeud" data-widget-id="625719222971334656">Tweets por el @ieeeud.</a>
								<script>
							!function(d,s,id){
								var js,fjs=d.getElementsByTagName(s)[0],
								p=/^http:/.test(d.location)?'http':'https';
								if(!d.getElementById(id)){js=d.createElement(s);
									js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
									fjs.parentNode.insertBefore(js,fjs);
								}
							}(document,"script","twitter-wjs");
							</script>
							</div>
						</div>

						<div class="small-12 medium-4 right columns recuadro" id="questions">
							<h3>Any Questions?</h3>
							<p>We have a team ready to help you with your questions, they can advise on most of the issues that you need. Join in and write us to know how we can help you</p>
							<a href="/en/contact/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Iconos/contacto.png" alt="Contact"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</section> <!-- Fin de main -->

<?php get_footer(); ?>
